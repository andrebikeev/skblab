#!/bin/bash
# Переменные
remote_branchlist=(`git branch --list -r`)
dictionary_of_exception_branches=("origin/master" "origin/develop")

printf "Before:\n"
printf '%s\n' "${remote_branchlist[@]}"
# Исключаем ветки master и develop
for (( i=0; i<${#remote_branchlist[@]}; i++ )); do
    if [[ ${remote_branchlist[i]} == ${dictionary_of_exception_branches[0]} || ${remote_branchlist[i]} == ${dictionary_of_exception_branches[1]} ]]; then
        remote_branchlist=( "${remote_branchlist[@]:0:$i}" "${remote_branchlist[@]:$((i + 1))}" )
        i=$((i - 1))
    fi
done

printf "After:\n"
printf '%s\n' "${remote_branchlist[@]}"
printf "\n"

# Проверяем регуляркой на соответствие именованию
for (( i=0; i<${#remote_branchlist[@]}; i++ )); do
    if [[ "${remote_branchlist[i]}" =~ ^origin\/(feature|bugfix)\/(TASK)-[[:digit:]]+  ]]; then
        valid_name_array+=(${remote_branchlist[i]})
    else
        invalid_name_array+=(${remote_branchlist[i]})
    fi
done

printf 'invalid_name_array %s\n' "${invalid_name_array[@]}"
printf 'valid_name_array %s\n' "${valid_name_array[@]}"
printf "\n"

# Уведомляем последнего контрибутора, что именование ветки неверное
for (( i=0; i<${#invalid_name_array[@]}; i++ )); do
  mail_last_contributor=(`git log ${invalid_name_array[i]} -1 --pretty=format:'%ae'`)
  name_last_contributor=(`git log ${invalid_name_array[i]} -1 --pretty=format:'%an'`)
  printf 'invalid_name_array %s\n' "${valid_name_array[i]}"
  printf "email=$mail_last_contributor name=$name_last_contributor branch=${invalid_name_array[i]}\n"
  #mail -s "Rename your branch" $mail_last_contributor <<< "Hello $name_last_contributor please rename your branch - ${invalid_name_array[i]}"
done

printf "\n"

# Проверяем для всех веток с правильным именем актуальную дату
for (( i=0; i<${#valid_name_array[@]}; i++ )); do
  date_last_update=(`git log ${valid_name_array[i]} -1 --pretty=format:'%at'`)
  date_now=`date +%s`
  #date_diff=$[( $date_now - $date_last_update )/(60*60*24)]
  date_diff=$[( $date_now - $date_last_update )/(60*60*24)]
  if [ "$date_diff" -lt 14 ]; then
    mail_last_contributor=(`git log ${valid_name_array[i]} -1 --pretty=format:'%ae'`)
    name_last_contributor=(`git log ${valid_name_array[i]} -1 --pretty=format:'%an'`)
    printf 'valid_name_array %s\n' "${valid_name_array[i]}"
    printf "email=$mail_last_contributor name=$name_last_contributor branch=${valid_name_array[i]}\n"
    #mail -s "Update your branch or delete this" $mail_last_contributor <<< "Hello $name_last_contributor please update or delete your branch - ${valid_name_array[i]}"
  fi
done
